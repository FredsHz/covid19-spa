import React from "react";
import Home from "./pages/home";
import notFound from "./pages/notFound";
import countryDetail from "./pages/countryDetail";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/country/:countryName" component={countryDetail} />
          <Route path="" component={notFound} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
