import React from "react";
import { useHistory } from "react-router-dom";
import { Form } from "react-bootstrap";

const SearchBar = ({ setSearchCountries, isDetail, search }) => {
  const history = useHistory();

  const handleClick = (countryName) => {
    if (isDetail) history.push("/", { searchQuery: countryName });
  };

  const handleOnChange = (countryName) => {
    if (isDetail) {
      history.push("/", { searchQuery: countryName });
    } else setSearchCountries(countryName);
  };

  return (
    <Form>
      <Form.Group controlId="formGroupSearch">
        <Form.Control
          style={{
            width: "85%",
          }}
          className="mx-auto searchBar"
          type="text"
          autoFocus
          placeholder="Buscar Pais..."
          value={search}
          onClick={(e) => handleClick(e.target.value)}
          onChange={(e) => handleOnChange(e.target.value)}
          // onChange={(e) => setSearchCountries(e.target.value)}
        />
      </Form.Group>
    </Form>
  );
};

export default SearchBar;
