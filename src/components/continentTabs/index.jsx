import React from "react";
import { Tabs, Tab, CardColumns } from "react-bootstrap";
import CountryCard from "../countryCard/index";

const ContinentTabs = ({ continent, continents, results, setContinent }) => {
  return (
    <Tabs
      activeKey={continent}
      onSelect={(continent) => setContinent(continent)}
    >
      {continents.map((continent, index) => (
        <Tab key={index} eventKey={continent} title={continent}>
          <CardColumns style={{ marginTop: 20 }}>
            {results
              .filter((item) => item.continent === continent)
              .map((data, i) => (
                <CountryCard key={i} countryData={data} />
              ))}
          </CardColumns>
        </Tab>
      ))}
    </Tabs>
  );
};

export default ContinentTabs;
