import React from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import NumberFormat from "react-number-format";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style.css";

const CountryCard = ({ countryData, searchCountry }) => {
  return (
    // <Link to={"/country/" + countryData.country}>
    <Link
      to={{
        pathname: `/country/${countryData.country}`,
        state: { searchQuery: searchCountry },
      }}
    >
      <Card className="text-center countryCard">
        <Card.Header as="h5">{countryData.country}</Card.Header>
        <Card.Body>
          <Card.Title style={{ fontWeight: "bolder", color: "black" }}>
            Population
          </Card.Title>
          <Card.Text className="text-secondary" style={{ fontWeight: "bold" }}>
            {"Total: "}
            <NumberFormat
              value={countryData.population}
              displayType={"text"}
              thousandSeparator={true}
            />
          </Card.Text>
          <Card.Text className="text-info" style={{ fontWeight: "bold" }}>
            {"Total Cases: "}

            <NumberFormat
              value={countryData.cases.total}
              displayType={"text"}
              thousandSeparator={true}
            />
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">
            Last Update: {Date(parseInt(countryData.time)).toString()}
          </small>
        </Card.Footer>
      </Card>
    </Link>
  );
};

export default CountryCard;
