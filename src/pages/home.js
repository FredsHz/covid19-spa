import React, { useEffect, useState } from "react";
import Axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, CardColumns } from "react-bootstrap";
import ContinentTabs from "../components/continentTabs/index";
import CountryCard from "../components/countryCard/index";
import SearchBar from "../components/searchBar/index";
import CircleLoader from "react-spinners/CircleLoader";
import "../App.css";

export default function Home({ location }) {
  const searchQuery =
    location.state !== undefined ? location.state.searchQuery : "";
  const [results, setResults] = useState([]);
  const [errorMsg, setErrorMsg] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchCountries, setSearchCountries] = useState(searchQuery);
  const [continent, setContinent] = useState("Africa");

  useEffect(() => {
    Axios.get("https://covid-193.p.rapidapi.com/statistics", {
      headers: {
        "x-rapidapi-host": "covid-193.p.rapidapi.com",
        "x-rapidapi-key": "1811e61d59msh26a32928d7dfed7p1ec227jsn0dd57c3078b4",
      },
    })
      .then((responseArr) => {
        setResults(responseArr.data.response);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setErrorMsg(err);
      });
  }, []);

  const allContinents = results
    .filter((item) => item.continent !== "All")
    .filter((item) => item.continent !== null)
    .map((item) => item.continent);

  let continents = allContinents.filter((item, index) => {
    return allContinents.indexOf(item) === index;
  });

  continents.sort();

  const filterCountries = results.filter((item) => {
    return searchCountries !== ""
      ? item.country.toLowerCase().includes(searchCountries.toLowerCase())
      : item;
  });

  return (
    <div className="App">
      <Container fluid>
        <div className="py-4 my-4">
          <h1>COVID-19 Live Stats</h1>
        </div>

        <SearchBar
          setSearchCountries={setSearchCountries}
          search={searchCountries}
        />

        <div
          style={{
            display: loading ? "flex" : "none",
            justifyContent: "center",
            marginBottom: 35,
            marginTop: 35,
          }}
        >
          <CircleLoader size={100} color={"green"} loading={loading} />
        </div>

        <div
          className="mx-auto"
          style={{
            display: searchCountries === "" ? "none" : "flex",
            width: "85%",
          }}
        >
          <CardColumns>
            {filterCountries.map((data, i) => (
              <CountryCard
                key={i}
                countryData={data}
                searchCountry={searchCountries}
              />
            ))}
          </CardColumns>
        </div>

        <div
          className="mx-auto"
          style={{
            display: searchCountries === "" ? "" : "none",
            width: "85%",
          }}
        >
          <ContinentTabs
            continent={continent}
            continents={continents}
            setContinent={setContinent}
            results={results}
          />
        </div>

        <div
          className="mx-auto mt-4 pt-4"
          style={{
            display: filterCountries.length > 0 ? "none" : "",
          }}
        >
          {!loading && (
            <h2>Lo sentimos. No se encontró ningún pais con ese nombre</h2>
          )}
          {errorMsg && <h2>{errorMsg}</h2>}
        </div>
      </Container>
    </div>
  );
}
