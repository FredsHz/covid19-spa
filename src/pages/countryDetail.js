import React, { useEffect, useState } from "react";
import { Container, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import Axios from "axios";
import SearchBar from "../components/searchBar/index";
import CircleLoader from "react-spinners/CircleLoader";
import NumberFormat from "react-number-format";
import "bootstrap/dist/css/bootstrap.min.css";

const CountryDetails = ({ match, location }) => {
  let { countryName } = match.params;
  let { searchQuery } = location.state;
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    Axios.get("https://covid-193.p.rapidapi.com/statistics", {
      headers: {
        "x-rapidapi-host": "covid-193.p.rapidapi.com",
        "x-rapidapi-key": "1811e61d59msh26a32928d7dfed7p1ec227jsn0dd57c3078b4",
      },
    })
      .then((responseArr) => {
        setResults(responseArr.data.response);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const countryDetail = results.filter((item) => item.country === countryName);
  return (
    <div className="App">
      <Container fluid>
        <div className="py-4 my-4 text-decoration-none">
          <Link to="/">
            <h1>COVID-19 Live Stats</h1>
          </Link>
        </div>

        <SearchBar isDetail={true} search={searchQuery} />

        <div
          style={{
            display: loading ? "flex" : "none",
            justifyContent: "center",
            marginBottom: 35,
            marginTop: 35,
          }}
        >
          <CircleLoader size={100} color={"green"} loading={loading} />
        </div>

        <div
          className="mx-auto"
          style={{
            width: "85%",
          }}
        >
          {countryDetail.map((item, i) => (
            <Card key={i} className="text-center">
              <Card.Header as="h1">{countryName}</Card.Header>
              <Card.Body>
                <Card.Title style={{ marginBottom: 15 }}>
                  {"Continent: " + item.continent}
                </Card.Title>
                <hr />
                <Card.Title
                  as="h3"
                  style={{ fontWeight: "bolder", color: "black" }}
                >
                  Population
                </Card.Title>
                <Card.Text
                  className="text-secondary"
                  style={{ fontWeight: "bold" }}
                >
                  {"Total: "}
                  <NumberFormat
                    value={item.population}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <hr style={{ width: "25%" }} />
                <Card.Title as="h3" style={{ fontWeight: "bolder" }}>
                  Cases
                </Card.Title>
                <Card.Text className="text-info" style={{ fontWeight: "bold" }}>
                  {"New Cases: "}
                  <NumberFormat
                    value={item.cases.new}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <Card.Text
                  className="text-primary"
                  style={{ fontWeight: "bold" }}
                >
                  {"Active Cases: "}
                  <NumberFormat
                    value={item.cases.active}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <Card.Text style={{ color: "orangered", fontWeight: "bold" }}>
                  {"Critical Cases: "}
                  <NumberFormat
                    value={item.cases.critical}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <Card.Text style={{ color: "green", fontWeight: "bold" }}>
                  {"Recovered Cases: "}
                  <NumberFormat
                    value={item.cases.recovered}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <Card.Text style={{ color: "#6C757D", fontWeight: "bold" }}>
                  {"Total Cases: "}
                  <NumberFormat
                    value={item.cases.total}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <hr style={{ width: "25%" }} />
                <Card.Title as="h3" style={{ fontWeight: "bolder" }}>
                  Deaths
                </Card.Title>
                <Card.Text
                  className="text-warning"
                  style={{ fontWeight: "bold" }}
                >
                  {"New: "}
                  <NumberFormat
                    value={item.deaths.new}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
                <Card.Text
                  className="text-danger"
                  style={{ fontWeight: "bold" }}
                >
                  {"Total: "}
                  <NumberFormat
                    value={item.deaths.total}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <small className="text-muted">
                  Last Update: {Date(parseInt(item.time)).toString()}
                </small>
              </Card.Footer>
            </Card>
          ))}
        </div>
      </Container>
    </div>
  );
};

export default CountryDetails;
