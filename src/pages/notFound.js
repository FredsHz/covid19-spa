import React from "react";

const notFound = () => {
  return (
    <div className="img-fluid text-center">
      <img
        style={{
          maxWidth: "100%",
          maxHeight: 900,
        }}
        alt="404 Page not Found"
        src="https://miracomosehace.com/wp-content/uploads/mch/id_2124.jpg"
      />
    </div>
  );
};

export default notFound;
